import angular from 'angular';
import uiRouter from 'angular-ui-router';
import pageComponent from './page.component';

const pageModule = angular.module('page', [
  uiRouter
])

.component('page', pageComponent);

export default pageModule;
