class PreFooterController {
  constructor(LinkService) {
  	"ngInject";

  	// This will keep the service instance across our class
  	this.LinkService = LinkService;
  	this.items = []; 
  }

  // This method will be called each time the component will be initialised,
  // In our case, it will be called for every page route change.
	$onInit(){
		 var that = this;
	     this.LinkService.getLinks().then(function(data) {
	     		that.items = data;
		 });
	}
}

export default PreFooterController;

