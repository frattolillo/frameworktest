import template from './preFooter.html';
import controller from './preFooter.controller';
//import './preFooter.scss';

let preFooterComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default preFooterComponent;
