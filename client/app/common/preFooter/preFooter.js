import angular from 'angular';
import uiRouter from 'angular-ui-router';
import preFooterComponent from './preFooter.component';

const preFooterModule = angular.module('preFooter', [
  uiRouter
])

.component('preFooter', preFooterComponent);

export default preFooterModule;
