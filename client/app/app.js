import angular      from 'angular';
import uiRouter     from 'angular-ui-router';
import ngAria       from 'angular-aria';
import AppComponent from './app.component';

import NavigationComponent from './common/navigation/navigation';
import ContactComponent           from './common/contact/contact';
import PageComponent           from './common/page/page';
import PreFooterComponent from './common/preFooter/preFooter';
import FooterComponent from './common/footer/footer';

// Pages
import HomeComponent from './pages/home/home';

import MarketingService from './services/MarketingService';
import LinkService from './services/LinkService';
import PageService from './services/PageService';

// import our default styles for the whole application
//import 'normalize.css';
//import 'bootstrap/dist/css/bootstrap.css';
import './styles/app.scss';

angular
    .module('app', [
        uiRouter,
        require('angular-aria'),
        NavigationComponent.name,
        ContactComponent.name,
        PreFooterComponent.name,
        FooterComponent.name,
        HomeComponent.name,
        PageComponent.name
    ])
    .config(($locationProvider, $stateProvider, $urlRouterProvider) => {
        "ngInject";

        // Define our app routing, we will keep our layout inside the app component
        // The layout route will be abstract and it will hold all of our app views
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                template: '<app></app>'
            })

            // Dashboard page to contain our goats list page
            .state('app.home', {
                url: '/home',
                template: '<home></home>'
            })

            // Create route for our goat listings creator
            .state('app.poker', {
                url: '/poker',
                template: '<page></page>'
            })
            // Create route for our goat listings creator
            .state('app.casino', {
                url: '/casino',
                template: '<page></page>'
            })
            // Create route for our goat listings creator
            .state('app.sports', {
                url: '/sports',
                template: '<page></page>'
            })
            // Create route for our goat listings creator
            .state('app.live', {
                url: '/live',
                template: '<page></page>'
            });

        $urlRouterProvider.otherwise('/app/home');
    })
    .component('app', AppComponent)
    .factory('MarketingService', MarketingService)
    .factory('LinkService', LinkService)
    .factory('PageService', PageService);