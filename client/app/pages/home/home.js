import angular                    from 'angular';
import homeComponent              from './home.component';
import BannerComponent            from '../../components/banner/banner';
import MarketingMessageComponent  from '../../components/marketingMessage/marketingMessage';
import ChannelsComponent          from '../../components/channels/channels';
import ShowMoreComponent          from '../../components/showMore/showMore';

let homeModule = angular.module('home', [
    BannerComponent.name,
    MarketingMessageComponent.name,
    ChannelsComponent.name,
    ShowMoreComponent.name
])

.component('home', homeComponent);

export default homeModule;
