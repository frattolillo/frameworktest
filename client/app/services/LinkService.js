function LinkService($http, $timeout) {
    "ngInject";

    var links = {
        getLinks: function() {
            return $timeout(function() {
                return $http.get('./app/data/links.json').then(function(response) {
                    return response.data;
                });
            }, 50);
        }
    }

    return links;

}

LinkService.$inject = ['$http','$timeout'];

export default LinkService;