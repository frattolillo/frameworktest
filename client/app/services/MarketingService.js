function MarketingService($http, $timeout) {
    "ngInject";

    var messages = {
        getMessages: function() {
            return $timeout(function() {
                return $http.get('./app/data/messages.json').then(function(response) {
                    return response.data;
                });
            }, 50);
        }
    }

    return messages;

}

MarketingService.$inject = ['$http','$timeout'];

export default MarketingService;