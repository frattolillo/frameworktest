function PageService($http, $timeout) {
    "ngInject";

    var contents = {
        getContents: function() {
            return $timeout(function() {
                return $http.get('./app/data/contents.json').then(function(response) {
                    return response.data;
                });
            }, 50);
        }
    }

    return contents;

}

PageService.$inject = ['$http','$timeout'];

export default PageService;