import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bannerComponent from './banner.component';

const bannerModule = angular.module('banner', [
  uiRouter
])

.component('banner', bannerComponent);

export default bannerModule;
