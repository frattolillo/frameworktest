import template from './banner.html';
import controller from './banner.controller';
//import './_banner.scss';

let bannerComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default bannerComponent;
