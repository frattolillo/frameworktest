import './toggleMore.scss';

let toggleMoreDirective = function() {
	
	function link(scope, elem, attrs) {

		elem.on('click', function() {
			$(elem).parent().find('.more').slideToggle()
		});

	}
	return {
		link: link
	}
}

export default toggleMoreDirective;
