import angular from 'angular';
import uiRouter from 'angular-ui-router';
import showMoreComponent from './showMore.component';
import toggleMoreDirective from './toggleMore.directive';

const showMoreModule = angular.module('showMore', [
  uiRouter
])

.component('showMore', showMoreComponent)
.directive('toggleMore', toggleMoreDirective);

export default showMoreModule;
