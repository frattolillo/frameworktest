import ShowMoreModule from './showMore'
import ShowMoreController from './showMore.controller';
import ShowMoreComponent from './showMore.component';
import ShowMoreTemplate from './showMore.html';

describe('ShowMore', () => {
  let $rootScope, makeController;

  beforeEach(window.module(ShowMoreModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new ShowMoreController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(ShowMoreTemplate).to.match(/{{\s?vm\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = ShowMoreComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(ShowMoreTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(ShowMoreController);
      });
  });
});
