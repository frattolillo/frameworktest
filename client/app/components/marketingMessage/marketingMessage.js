import angular from 'angular';
import uiRouter from 'angular-ui-router';
import marketingMessageComponent from './marketingMessage.component';

const marketingMessageModule = angular.module('marketingMessage', [
  uiRouter
])

.component('marketingMessage', marketingMessageComponent);

export default marketingMessageModule;
