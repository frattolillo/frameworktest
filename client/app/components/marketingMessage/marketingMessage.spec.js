import MarketingMessageModule from './marketingMessage'
import MarketingMessageController from './marketingMessage.controller';
import MarketingMessageComponent from './marketingMessage.component';
import MarketingMessageTemplate from './marketingMessage.html';

describe('MarketingMessage', () => {
  let $rootScope, makeController;

  beforeEach(window.module(MarketingMessageModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new MarketingMessageController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(MarketingMessageTemplate).to.match(/{{\s?vm\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = MarketingMessageComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(MarketingMessageTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(MarketingMessageController);
      });
  });
});
