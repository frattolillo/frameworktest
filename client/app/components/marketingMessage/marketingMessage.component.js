import template from './marketingMessage.html';
import controller from './marketingMessage.controller';
//import './marketingMessage.scss';

let marketingMessageComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default marketingMessageComponent;
