class MarketingMessageController {
  constructor(MarketingService) {
  	"ngInject";

    // This will keep the service instance across our class
    this.MarketingService = MarketingService;
  	this.messages = [];
  }

  // This method will be called each time the component will be initialised,
  // In our case, it will be called for every page route change.
	$onInit(){
		 var that = this;
	     this.MarketingService.getMessages().then(function(data) {
	     		that.messages = data;
		 });

	}

}

export default MarketingMessageController;
