# The Framework Test Component based - angular v. 1.5.9 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

node v6.9.2 and npm 3.10.9

### Dependencies

Install globally Webpack and Gulp: 

1) npm install webpack gulp --global
2) npm install

### Run the application typing the following command from your terminal

gulp

## Structure

The entire app lives in ./client/app folder

### Main Component

App Component is the main Component for bootstrapping the application.

### Common

All layout/template components shared in the app such as Navigation, Contact, PreFooter, Footer and Page. 

### Components

All block components you can find in the homepage such as Banner, Channels, MarketingMessage and ShowMore blocks.

### Pages

Specific page components such as Home live here.

### Services

All services including Link Service, Marketing Service and Page Service.

### Styles

All high level styles, more specific styles are part of each components

### Images

Body Background, logo, main banner and sprites.

### Data

json files in order to inject data into MarketingMessage, PreFooter and Page Components.


